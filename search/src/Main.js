import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AboutView from './About'
import SearchList from './SearchList'
import AddView from './Add'



function Main() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={SearchList}/>
          <Route path='/about' component={AboutView}/>
          <Route path='/add' component={AddView}/>
        </Switch>
      </main>
    );
  }

  export default Main