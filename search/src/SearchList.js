import React, { useState, useEffect } from "react";
// import ReactDOM from 'react-dom';
// import SearchField from 'react-search-field';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import SearchBar from "material-ui-search-bar";
import MoreInformation from "./MoreInformation";
import Grid from "@material-ui/core/Grid";
import "typeface-roboto";
// List Items
// import Drawer from "@material-ui/core/Drawer";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
// import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
// import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
// import IconButton from "@material-ui/core/IconButton";
//import ListItemIcon from '@material-ui/core/ListItemIcon';
// import Typography from "@material-ui/core/Typography";
import ListItemText from "@material-ui/core/ListItemText";
// import Divider from "@material-ui/core/Divider";
import BuildIcon from "@material-ui/icons/Build";
import BuildOutlineIcon from "@material-ui/icons/BuildOutlined";
// import EditAttributesIcon from "@material-ui/icons/EditAttributes";
import Badge from "@material-ui/core/Badge";
//import TemporaryDrawer from './SideBarDetail'
// import placeholder from "./placeholder.jpg";
import Fade from "@material-ui/core/Fade";
// const dotenv = require('dotenv');

const styles = theme => ({
  root: {
    width: "100%",
    maxWidth: 600,
    backgroundColor: theme.palette.background.paper
  }
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

function BuildIconStock(props) {
  // console.log("lets set our icon")
  const CurrentIcon = ((props.stockLevel).toString() !== "0" ) ? <BuildIcon/> : <BuildOutlineIcon/>;
  return(CurrentIcon);
}


function SearchList() {
  // const [test, setTest] = useState(0);
  const [dataSource, setDataSource] = useState([]);
  const [items, setItems] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [searchTerm, setSearchTerm] = useState("placehold");
  const [drawer, setDrawer] = useState(false);
  const [drawerId, setDrawerId] = useState(false);
  // const [drawerContent, setDrawerContent] = useState("");
  // const [editStock, setEditStock] = useState(false);
  // class SearchList extends Component {
  //   constructor(props) {
  //       super(props);

  //       this.state = {
  //         dataSource: [],
  //         items: [],
  //         error: null,
  //         isLoaded: false,
  //         searchTerm: 'placehold',
  //         drawer: false,
  //         drawerContent: '',
  //         editStock: false
  //       }
  //   }

  useEffect(() => {
    document.title = "Inventorious - for glory!";
    fetch(process.env.REACT_APP_PARTS_URL)
      .then(res => res.json())
      .then(
        result => {
          setIsLoaded(true);
          setDataSource(result);
          setItems(result); // Show all on startup
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  // componentDidMount() { // This will only run once
  useEffect(() => {
    document.title = "Inventorious - for glory!";
      fetch(process.env.REACT_APP_PARTS_URL)
        .then(res => res.json())
        .then(
          result => {
            setIsLoaded(true);
            setDataSource(result);
            setItems(result); // Show all on startup
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          error => {
            setIsLoaded(true);
            setError(error);
          }
        );
  }, [searchTerm]);

  const handleUpdate = value => {
    // If we don't want to search unless there is enough data.
    if (value.length < 0) {
      return false;
    }
    let processedList = dataSource
      .filter(function(result) {
        return result.name.toLowerCase().includes(value.toLowerCase());
      })
      .sort();
    setItems(processedList);
    //  this.setState({items: processedList});

    document.title = `Inventorious - search for: ${value}`;

  };




  const toggleDrawer = (open, id) => () => {
    // Set Message:
    setDrawer(open);
    setDrawerId(id);
  };

  // EditStock = ( open, id) => () => {
  //   // Set Message:
  //   setEditStock(open)
  //   setEditStockId(id)
  // });
  // }

  // Main hook

  // Search results
  const results = items.map((item, id) => {
    const description = item.name ? item.name : "no results found";
    const locationSimple = item.location;
    const stockLevel = item.stockLevel;
    const globalId = item.id;
    return (
      <Fade in={true} key={item.id}>
        <ListItemLink
          button
          key={globalId}
          data-key={globalId}
          href="#"
          onClick={toggleDrawer(true, item.id)}
        >
          <Badge
            color="secondary"
            badgeContent={stockLevel ? stockLevel : ""}
          >
            <BuildIconStock stockLevel={stockLevel} />
          </Badge>
          <ListItemText primary={description} secondary={locationSimple} />
          {/* The goal here is to allow for editable stock levels, should be easy to implement! 
            <ListItemSecondaryAction>
                <IconButton edge="end" onClick={this.EditStock(true, item.id)} aria-label="Edit stock level">
                  <EditAttributesIcon/>
                </IconButton>
              </ListItemSecondaryAction> */}
        </ListItemLink>
      </Fade>
    );
  });

  const sideList = (
    <React.Fragment>
      <div style={{ minWidth: "200px" }}>
        <MoreInformation 
        id={drawerId} 
        data={dataSource[drawerId]}
        />
      </div>{" "}
    </React.Fragment>
  );
  return (
    <MuiThemeProvider>
      <SearchBar
        onChange={value => handleUpdate(value)}
        onRequestSearch={value => handleUpdate(value)}
        style={{
          margin: "0 auto",
          marginTop: ".75em",
          maxWidth: 800
        }}
      />
      <Grid container spacing={24} justify="center">
        <Grid item xs={12} lg={10}>
          <div>
            <List component="nav">{results}</List>
          </div>
        </Grid>
      </Grid>
      <SwipeableDrawer
        anchor="right"
        open={drawer ? drawer : false}
        onClose={toggleDrawer(false, drawerId)}
      >
        <div
          tabIndex={0}
          role="button"
          onKeyDown={toggleDrawer(false, drawerId)}
        ></div>
        {sideList}
      </SwipeableDrawer>
    </MuiThemeProvider>
  );

}

export default withStyles(styles)(SearchList);
