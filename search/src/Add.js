
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Locations from './Locations';
import AddLocationDialog from './AddLocationDialog';
import Fade from '@material-ui/core/Fade';
// import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  stockField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 50,
  },
  locationField: {
    marginLeft: theme.spacing.unit,
    marginRight: 0,
    width:200
  },
  heading: {
    margin: theme.spacing.unit * 2,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});


class AddView extends React.Component {
  constructor(props) { 
    super(props);
    this.state = {
      dataSource: [],
      error: null,
      isLoaded: false,
      name: '',
      stockLevel: '1',
      newestItem: null,
      location: 'Wooden Rack - labeled',
      extraLocation: null
  
    };
  
  
  }

  simplifiedFunction (value) {
    console.log(value)
  }

  newTemporaryLocation = (locationFromDialog) => {
    console.log(locationFromDialog);
    this.setState({
      extraLocation: locationFromDialog
    });

    
}


  componentDidMount() {
    document.title = "Inventorious - Add new item!";
  
    fetch( process.env.REACT_APP_PARTS_URL )
      .then(res => res.json())
      .then(
        (result) => {
          //console.log(result);
          const newId = result.length;
          // console.log(newId);

          this.setState({
            isLoaded: true,
            dataSource: result,
            id: newId
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }  

  
  addOption = (option) => {
    // this.setState({
    //   fieldVal: val
    // })
    console.log('parent says woop');
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = event => {
    // this.setState({
    //     id: newId
    // });

//   console.log('A name was submitted: ' + this.state.id);

  event.preventDefault();
  // const data = new FormData(event.target);
// console.log(data);
  let item = JSON.stringify({
    name: this.state.name,
    id: (this.state.id).toString(),
    location: this.state.location,
    stockLevel: this.state.stockLevel
});
// console.log(item);

// Send data to server
    fetch( process.env.REACT_APP_PARTS_URL , {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method:'POST',
        body: item
    }).then(res => res.json()).then(
      (result) => {
        // console.log(result)
        this.setState({
          isLoaded: true,
          newestItem: result.id
        });
      },
      (error) => {
        this.setState({
          isLoaded: false,
          error
        });
      }
    ); 

    // We are done, reset the name to make sure they don't try to double submit. Don't change the location though.
    let stockName = document.querySelector("#stock-name");
    stockName.value = "";

    // Get the data back to verify and show it
    fetch( process.env.REACT_APP_PARTS_URL )
    .then(res => res.json())
    .then(
      (result) => {
        //console.log(result);
        const newId = result.length;

        // const hasMatch = result.find(function(element) {
        //   return (element.id).toString() === (newId).toString();
        // })      

        // // console.log(` Lets change the newest for ${this.state.newestItem}, oh btw ${hasMatch}`)
        // console.log(hasMatch);
        // console.log("this is bad")
          

        this.setState({
          isLoaded: true,
          dataSource: result,
          id: newId,
          name: '',
          extraLocation: null
          
        });

        // console.log(`We've finished, and it was a success. Let's show it! ${(this.state.newestItem).toString()}`)
        let newestItem = document.querySelector(`#allitems li[editid='${this.state.newestItem}']`);
        // console.log(newestItem)
        newestItem.style.backgroundColor = '#BBFFBB';
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )


    // this.setState({"newestItem": newId})
}  

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Typography variant="h2" color="inherit" className={classes.heading}>
            Add
        </Typography>
      <form id="addForm" className={classes.container} onSubmit={this.handleSubmit } autoComplete="off">

        <TextField
        required
          id="stock-name"
          label="Name"
          placeholder="Searchable name of it"
          className={classes.textField}
          onChange={this.handleChange('name')}
          margin="normal"
          InputProps={{ inputProps: { tabIndex: 1 } }}
        />

        <TextField
          id="stock-number"
          label="Stock"
          value={this.state.age}
          onChange={this.handleChange('stockLevel')}
          defaultValue="1"
          type="number"
          className={classes.stockField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          InputProps={{ inputProps: { tabIndex: 2 } }}
        />

        <TextField
          id="location-select"
          select
          label="Location"
          className={classes.locationField}
          value={this.state.location}
          onChange={this.handleChange('location')}
          SelectProps={{
            native: true,
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please choose where it is"
          margin="normal"
          InputProps={{ inputProps: { tabIndex: 3 } }}
          
        >
          <Locations items={this.state.dataSource} extraLocation={this.state.extraLocation} />
        </TextField>
        <AddLocationDialog
        callbackFromParent={this.newTemporaryLocation}
        />
        
        {/* <TextField
          id="standard-full-width"
          label="Label"
          style={{ margin: 8 }}
          placeholder="Placeholder"
          helperText="Full width!"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        /> */}
        <Divider/>
        <input type="submit" value="Submit" tabIndex="4" />
      </form>
      <AllItems id="allitems" items={this.state.dataSource} justadded={this.state.id}/>
      </div>
    );
  }
}

class AllItems extends Component {
  
  render() {
    // let newItemCheck = (typeof this.props.justadded === 'undefined') ? true : false
    // console.log(this.props.items);
    // console.log(`So we're checking on ${this.props.justadded}, and should we check? ${newItemCheck}`);

      return(
          <ul id={this.props.id} style={{columnCount: "4"}}>
              {
                  this.props.items.map((item, index) => (
                  
                  <Fade in={true} key={item.id}>
                    <li 
                    title={`location: ${item.location}, and key: ${item.id}`}
                    style={
                      {
                        color: (item.stockLevel.toString() === "0") ? "red" : "initial",
                        // background: ( newItemCheck && (20).toString() === (item.id).toString()) ? "#ABFFAB" : "blue"
                      }
                    }
                    key={item.id}
                    editid={item.id}
                    >
                    {item.name}: <span style={
                      {
                      color: (item.stockLevel.toString() === "0") ? "red" : "grey",
                      fontWeight:"light", 
                      fontSize:".8rem", 
                      fontStretch:"condensed",
                      }

                      }>{item.stockLevel}</span>
                    </li>
                  </Fade>
                  ))
              }
          </ul>
      )
  }
}


AddView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddView);