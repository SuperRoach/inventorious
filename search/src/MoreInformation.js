import React, { useState, useEffect } from "react";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Image from "material-ui-image";
import placeholder from "./placeholder.jpg";

const MoreInformation = props => {
  const [locationSimple, setLocationSimple] = useState("placehold");
  const [outofStock, setOutOfStock] = useState(false);
  const [description, setDescription] = useState("no results found");
  const [stockLevel, setStockLevel] = useState(0);
  const [moreInfo, setMoreInfo] = useState("");
  const [data, setData] = useState({});
  const [moreContent, setMoreContent] = useState([]);
  const [image, setImage] = useState(false);
  // function MoreInformation(...props) {
  // Declare a new state variable, which we'll call "count"

  useEffect(() => {
    setData(props.data);
  }, []);

  useEffect(() => {
    if (data !== null && data.name) {
      console.log("footime", data, data !== null);
      setDescription(data.name ? data.name : "no results found");
      setLocationSimple(data.location);
      setStockLevel(data.stockLevel); //Math.floor(Math.random() * 4); // we have it with item.stockLevel but lets make it look fun for demonstration
      // console.log(data);
      setOutOfStock(data.stockLevel.toString() !== "0");
      setMoreInfo(data.hasOwnProperty("moreInfo"));
      setImage(data.hasOwnProperty("imageURL") ? data.imageURL : placeholder);

      if (data.moreInfo) {
        // setMoreContent([]);
        Object.entries(data.moreInfo).forEach(function(item, index) {
          console.log(`We're looking into showing ${index}, who is ${moreContent}`);
          // console.log(item);
          if (typeof item[1] === "string") {
            setMoreContent(
              moreContent.concat(`<li>${item[0]}: ${item[1]}</li>`)
            );
          }
        });

        setMoreContent(moreInfoRender(data.moreInfo));
      }
    }
  }, [data]);

  function DrawerImage(image) {
    // const { error, isLoaded, items, drawerId } = this.state;
    // Import result is the URL of your image
    // return (

    // <img src={placeholder} alt="Part picture"
    // style={{flex:1, height: undefined, width: undefined}}
    // />
    // )
    return (
      <div>
        <Image
          // src="http://loremflickr.com/200/150"
          src={image}
        />
      </div>
    );
  }

  function moreInfoRender(details) {
    /**
     * Iterates over the details object of an item, and if present add that to the display
     */
    let info = Object.entries(details);
    return (
      <div>
        {info.map((item, index) => (
          <li key={index}>
            {item[0]}: {item[1]}
          </li>
        ))}
      </div>
    );
  }

  return (
    <div style={{ margin: "1rem", minWidth: "100px" }}>
      <Typography variant="h4" color="inherit">
        {description}
      </Typography>
      <div color="primary">Ballarat hackerspace</div>
      <Divider />
      <ul>
        <li>
          Stock: {stockLevel}
          {outofStock ? null : (
            <span>
              {" "}
              - oops, Out of stock!{" "}
              <span role="img" aria-label="sad face">
                😔
              </span>{" "}
            </span>
          )}
        </li>
        <li>Location: {locationSimple}</li>
      </ul>
      <div
        style={{
          margin: "0 auto",
          minWidth: "300px",
          maxWidth: "350px"
        }}
      >
        {DrawerImage(image)}
      </div>
      <ul className="moreInfo">{moreContent}</ul>

      <Divider />
      {/* <button>I'd like more of this!</button> */}
    </div>
  );
};

export default MoreInformation;
