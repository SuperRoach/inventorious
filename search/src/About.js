import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    heading: {
        margin: theme.spacing.unit * 2,
      },
      snazzy:{
          margin: theme.spacing.unit
      }
  });
  

class AboutView extends Component {
    render() {
        const { classes } = this.props; 
        return (
            <div>
            <Typography variant="h2" color="inherit" className={classes.heading}>
            About
            </Typography>
                <div className={classes.heading}>
                    <p>
                    This is called Inventorious, 
                    a bare bones inventory system that lets 
                    your users see what you have in stock quickly.
                    </p>
                    <p>
                        It is fast to mock your data-set up by using json-server. 
                        You can see this by visiting <a href={process.env.REACT_APP_PARTS_URL}>{process.env.REACT_APP_PARTS_URL}</a> on the stock configuration.
                    </p>
                </div>
            </div>
    )
    };
}

// export default AboutView;
AboutView.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(AboutView);