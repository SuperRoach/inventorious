import React from 'react'
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import red from '@material-ui/core/colors/red';
// import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
// import AddIcon from '@material-ui/icons/AddCircle';
import HelpIcon from '@material-ui/icons/Help';
// import SearchIcon from '@material-ui/icons/Search';
import IconButton from 'material-ui/IconButton';
import PropTypes from 'prop-types';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// import white from '@material-ui/core/colors/lightBlue';


const styles = theme => ({
  // This group of buttons will be aligned to the right
  rightToolbar: {
    marginLeft: 'auto',
    marginRight: -12,
  },
  menuButton: {
    marginRight: 16,
    marginLeft: -12,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});


// The Header creates links that can be used to navigate
// between routes.

const Header = ({ classes }) => {
  
    return (
      <MuiThemeProvider>
      <header>
        <AppBar position="static" color="primary">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Inventorious
            </Typography>

    <section className={classes.rightToolbar}>
      <Link to='/'>
        <Button variant="contained" className={classes.button}>
          Find
        </Button>    
      </Link>          
      <Link to='/add'>
        <Button variant="contained" color="secondary" className={classes.button}>
          Add
        </Button>       
      </Link>
      <Link to='/about'>     
        <IconButton color="inherit"  aria-label="Edit"  >
        <HelpIcon color="action" />
      </IconButton>
      </Link>
    </section>                

          </Toolbar>
          
        </AppBar>
      </header>
      </MuiThemeProvider>
    );
  }  

  Header.propTypes = {
    classes: PropTypes.object.isRequired,
  };  
  

// export default Header
export default withStyles(styles)(Header);
