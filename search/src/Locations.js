import React, { Component } from 'react';

class Locations extends Component {
    constructor(props) { 
      super(props);
      this.state = {         
        locations: [
                   'Please fill me in'
                ],
        // The purpose of a default is to give the user a starting set of locations. 
          defaults: [
            'Blue toolbox',
            'Wooden Rack - labeled',
            'Wooden Rack - open drawers',
            'Soldering Station',
            'at the Space',
          ]
      }
    }

    
    componentDidMount() {        
        // Set the defaults
        this.setState({
            locations: this.state.defaults
        })
        // this.state.locations = this.state.defaults;
    }

    render() {
        // Grab the items we have from our dynamic list of items, and get the locations that have been set in the past
        let locationsToAdd = [];
        // console.log(this.props.items);
    if (this.props.hasOwnProperty('items')) {
        const dynamicLocations = this.props.items.map(item => item.location);
        // De-duplicate them.
        // const unique = [...new Set(dynamicLocations)];
        // Combine our defaults with the dynamic list of items to give our list to populate
        locationsToAdd = [...new Set([...dynamicLocations ,...this.state.locations])];
    } else locationsToAdd = this.state.locations

    // Do we have an extra user specified location to add?
    if (this.props.hasOwnProperty('extraLocation') && this.props.extraLocation !== null) {
        if (this.props.extraLocation.length >= 1) {
            locationsToAdd.unshift(this.props.extraLocation)
        }
    }
        
      return (
        locationsToAdd
        .sort()
        .map(option => (
          <option key={option} value={option}>
            {option}
          </option>
        ))
      );
    }
  }

  export default Locations